package application;

import java.util.Calendar;

public class TimeUtils {
    private long lastFrame;
    private long currentFrame;
    private static TimeUtils instance;

    private TimeUtils() {
        lastFrame = getTime();
        currentFrame = getTime();
    }

    public static TimeUtils getInstance() {
        if (instance == null)
            instance = new TimeUtils();
        return instance;
    }

    public long getDelta() {
        return currentFrame - lastFrame;
    }

    public float getDeltaSeconds() {
        return (float) getDelta() / 1000;
    }

    public void update() {
        lastFrame = currentFrame;
        currentFrame = getTime();
    }

    private long getTime() {
        return Calendar.getInstance().getTimeInMillis();
    }
}

