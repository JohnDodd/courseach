package entity.bees;

import org.joml.Vector2f;

import java.util.Random;

import entity.Transform;
import io.Window;
import render.Animation;
import render.Camera;
import world.World;

public class BeeDefender extends AbstractBee {
    public static final int ANIM_IDLE = 0;
    public static final int ANIM_WALK = 1;
    public static final int ANIM_SIZE = 2;
    private float time;
    private int speed;
    private int stayTime;
    private boolean flyingSide;
    private Vector2f randomPoint;
    private Vector2f origin;

    public BeeDefender(Transform transform) {
        super(ANIM_SIZE, transform);
        setAnimation(ANIM_IDLE, new Animation(4, 8, "bees/defender/idle"));
        setAnimation(ANIM_WALK, new Animation(4, 8, "bees/defender/walking"));
        time = 0;
        speed = 2 + random.nextInt(2);
        stayTime = 1 + random.nextInt(2);
        random = new Random();
        flyingSide = false;
        randomPoint = new Vector2f();
        origin = new Vector2f().add(transform.pos.x, transform.pos.y);
        updatePoint();
    }

    private void updatePoint() {
        randomPoint.x = origin.x + (-4 + random.nextInt(8));
        randomPoint.y = origin.y + (-4 + random.nextInt(8));
    }

    @Override
    public void update(float delta, Window window, Camera camera, World world) {
        super.update(delta, window, camera, world);
        flyTo(randomPoint, delta);
    }

    protected void flyTo(Vector2f vector, float delta) {
        float x = transform.pos.x;
        float y = transform.pos.y;
        float absX = Math.abs(vector.x - transform.pos.x);
        float absY = Math.abs(vector.y - transform.pos.y);

        if (vector.x > transform.pos.x) {
            if (absX > absThreshold) {
                flipRight();
                speedX = speed * delta;
            } else speedX = 0;
        } else {
            if (absX > absThreshold) {
                flipLeft();
                speedX = -speed * delta;
            } else speedX = 0;
        }

        if (vector.y > transform.pos.y)
            speedY = speed * delta;
        else speedY = -speed * delta;
        if (absY <= 0) speedY = 0;

        if (absX < absThreshold)
            speedX = 0;
        if (absY < absThreshold)
            speedY = 0;


        if (absX < absThreshold && absY < absThreshold) {
            speedX = 0;
            speedY = 0;
            time += delta;
            if (time >= stayTime) {
                flyingSide = !flyingSide;
                time = 0;
                updatePoint();
            }
        }

        correctRotation();
        correctPosition(x, y);
    }
}
