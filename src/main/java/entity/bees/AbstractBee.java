package entity.bees;

import org.joml.Vector2f;
import org.joml.Vector3f;

import entity.Transform;
import entity.abstract_entities.AbstractEntity;
import entity.abstract_entities.AbstractMovableEntity;

public abstract class AbstractBee extends AbstractMovableEntity {
    public AbstractBee(int max_animations, Transform transform) {
        super(max_animations, transform);
    }

    protected void flyTo(AbstractEntity other, float delta) {
        float x = transform.pos.x;
        float y = transform.pos.y;
        float otherX = other.getTransform().pos.x;
        float otherY = other.getTransform().pos.y;
        float absX = Math.abs(otherX - x);
        float absY = Math.abs(otherY - y);

        Vector3f different = new Vector3f();
        other.getTransform().pos.sub(transform.pos, different);

        float tang = different.x / different.y;
        if (tang == Double.POSITIVE_INFINITY || tang > speed)
            tang = speed;
        if (tang == Double.NEGATIVE_INFINITY || tang < -speed)
            tang = -speed;
        System.out.println("tang: " + tang);

        if (otherX > x) {
            if (absX > absThreshold) {
                flipRight();
                speedX = speed * delta;
            } else speedX = 0;
        } else {
            if (absX > absThreshold) {
                flipLeft();
                speedX = -speed * delta;
            } else speedX = 0;
        }

        if (otherY > y)
            speedY = speed * delta;
        else speedY = -speed * delta;
        if (absY < absThreshold) speedY = 0;

        speedX *= Math.abs(tang);

        correctPosition(transform.pos.x, transform.pos.y);
        correctRotation();
    }

    protected void correctPosition(float x, float y) {
        if (x < 0) {
            transform.pos.x = 0;
            speedX = 0;
        }
        if (x > 18) {
            transform.pos.x = 18;
            speedX = 0;
        }
        if (-y < 0) {
            transform.pos.y = 0;
            speedY = 0;
        }
        if (-y > 13) {
            transform.pos.y = -13;
            speedY = 0;
        }
    }

    protected void correctRotation() {
        float angle = (float) Math.PI / 6;

        if (speedY > 0)
            rotate(angle);
        else if (speedY < 0)
            rotate(-angle);
        else rotate(0);
    }

}
