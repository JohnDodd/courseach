package entity.bees;

import entity.Transform;
import io.Window;
import render.Animation;
import render.Camera;
import world.World;

public class BeeLarva extends AbstractBee {
    public static final int ANIM_IDLE = 0;
    public static final int ANIM_SIZE = 2;
    private float time = 0;

    public BeeLarva(Transform transform) {
        super(ANIM_SIZE, transform);
        setAnimation(ANIM_IDLE, new Animation(4, 8, "bees/larva"));
    }

    @Override
    public void update(float delta, Window window, Camera camera, World world) {
        time += delta;
    }

    public boolean complete(){
        return time >= 5;
    }
}
