package entity.bees;

import collision.Collision;
import entity.Flower;
import entity.Transform;
import entity.abstract_entities.AbstractEntity;
import io.Window;
import render.Animation;
import render.Camera;
import world.World;

public class BeeWorker extends AbstractBee {
    public static final int ANIM_IDLE = 0;
    public static final int ANIM_WALK = 1;
    public static final int ANIM_HONEY = 2;
    public static final int ANIM_SIZE = 3;

    private float time;
    private Flower currentFlower;
    private boolean flyingToFlower;
    private int stayTime;
    private boolean dead = false;

    public BeeWorker(Transform transform) {
        super(ANIM_SIZE, transform);
        time = 0;
        setAnimation(ANIM_IDLE, new Animation(4, 8, "bees/worker/idle"));
        setAnimation(ANIM_WALK, new Animation(4, 8, "bees/worker/walking"));
        setAnimation(ANIM_HONEY, new Animation(4, 8, "bees/worker/walking/honey"));
        flyingToFlower = true;
        speed = 1.0f * (3 + random.nextInt(2));
        stayTime = 1 + random.nextInt(2);
    }

    @Override
    public void update(float delta, Window window, Camera camera, World world) {
        super.update(delta, window, camera, world);
        if (dead) {
            time += delta;
            fall(delta);
            useAnimation(ANIM_IDLE);
            rotate((float) Math.PI);
            return;
        }
        if (flyingToFlower) {
            if (currentFlower == null)
                currentFlower = world.getRandomFlower();
            flyTo(currentFlower, delta);
        } else {
            flyTo(Bees.getInstance().getEater(), delta);
            currentFlower = null;
        }
    }

    private void fall(float delta) {
        float x = transform.pos.x;
        float y = transform.pos.y;

        speedX = 0;
        accelerationY += delta * 0.01;

        correctRotation();
        correctPosition(x, y);
    }

    @Override
    protected void flyTo(AbstractEntity other, float delta) {
        super.flyTo(other, delta);
        if (collides(other)) {
            speedX = 0;
            speedY = 0;
            time += delta;
            if (time >= stayTime) {
                flyingToFlower = !flyingToFlower;
                time = 0;
            }
        }

        if (speedX == 0 && speedY == 0)
            time += delta;
        correctAnimation();
    }

    private void correctAnimation() {
        if (speedX != 0 || speedY != 0)
            if (flyingToFlower)
                useAnimation(ANIM_WALK);
            else
                useAnimation(ANIM_HONEY);
        else useAnimation(ANIM_IDLE);
    }

    @Override
    public void collideWithEntity(AbstractEntity entity) {
        Collision collision = boundingBox.getCollision(entity.getBoundingBox());

        if (collision.isIntersecting) {
            collision.distance.x /= 2;
            collision.distance.y /= 2;

            boundingBox.correctPosition(entity.getBoundingBox(), collision);
            transform.pos.set(boundingBox.getCenter().x, boundingBox.getCenter().y, 0);
        }
    }

    public void die() {
        dead = true;
        time = 0;
    }

    public boolean isClear() {
        return dead && time >= 5;
    }
}
