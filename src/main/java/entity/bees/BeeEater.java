package entity.bees;

import entity.Transform;
import io.Window;
import render.Animation;
import render.Camera;
import world.World;

public class BeeEater extends AbstractBee {
    public static final int ANIM_IDLE = 0;
    public static final int ANIM_SIZE = 1;
    private float time;

    public BeeEater(Transform transform) {
        super(ANIM_SIZE, transform);
        setAnimation(ANIM_IDLE, new Animation(4, 8, "bees/eater"));
        transform.scale.add(1, 1, 0);
    }

    @Override
    public void update(float delta, Window window, Camera camera, World world) {
        time += delta;
    }

    public boolean complete() {
        return time >= 4;
    }

    public void setTime(float time) {
        this.time = time;
    }
}
