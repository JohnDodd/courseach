package entity.bees;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import entity.Transform;
import io.Window;
import render.Camera;
import render.Shader;
import world.World;

public class Bees {
    private List<BeeWorker> workers;
    private List<BeeLarva> larvas;
    private List<BeeDefender> defenders;
    private BeeEater eater;
    private Random random;

    private static Bees instance;

    public static Bees getInstance() {
        if (instance == null)
            instance = new Bees();
        return instance;
    }

    private Bees() {
        random = new Random();
        workers = new ArrayList<>();
        larvas = new ArrayList<>();
        defenders = new ArrayList<>();
    }

    public BeeEater getEater() {
        return eater;
    }

    public Bees setEater(BeeEater eater) {
        this.eater = eater;
        addBee(eater);
        return this;
    }

    public Bees addBee(AbstractBee bee) {
        if (bee instanceof BeeWorker)
            workers.add((BeeWorker) bee);

        if (bee instanceof BeeLarva)
            larvas.add((BeeLarva) bee);

        if (bee instanceof BeeDefender)
            defenders.add((BeeDefender) bee);
        if (bee instanceof BeeEater)
            eater = (BeeEater) bee;
        return this;
    }

    public void update(float delta, Window window, Camera camera, World world) {
        updateEater();
        eater.update(delta, window, camera, world);
        for (BeeWorker bee : workers)
            bee.update(delta, window, camera, world);
        for (BeeLarva bee : larvas)
            bee.update(delta, window, camera, world);
        for (BeeDefender bee : defenders)
            bee.update(delta, window, camera, world);


        for (BeeDefender defender : defenders) {
            for (int i = 0; i < workers.size(); i++) {
                BeeWorker worker = workers.get(i);
                if (worker.collides(defender)) {
                    worker.die();
                    worker.collideWithEntity(defender);
                }
                if (worker.isClear()) {
                    workers.remove(i);
                }
            }
        }

        for (int i = 0; i < workers.size(); i++) {
            BeeWorker worker = workers.get(i);
            if (worker.isClear()) {
                workers.remove(i);
            }
        }

        for (int i = 0; i < larvas.size(); i++) {
            BeeLarva larva = larvas.get(i);
            if (larva.complete()) {
                larvas.remove(i);
                addBee(new BeeWorker(new Transform(larva.getTransform())));
            }
        }
    }

    private void updateEater() {
        if (eater.complete()) {
            addBee(new BeeLarva(new Transform(
                    eater.getTransform().pos.x + random.nextInt(2),
                    eater.getTransform().pos.y - random.nextInt(2))));
            eater.setTime(0);
        }
    }

    public void render(Shader shader, Camera camera, World world) {
        eater.render(shader, camera, world);
        for (BeeWorker bee : workers)
            bee.render(shader, camera, world);
        for (BeeLarva bee : larvas)
            bee.render(shader, camera, world);
        for (BeeDefender bee : defenders)
            bee.render(shader, camera, world);
    }

    public void fill() {
        Bees.getInstance().setEater(new BeeEater(new Transform(16, -2)));
        for (int i = 0; i < 3 + random.nextInt(2); i++) {
            int x = 3 + random.nextInt(17);
            System.out.println(x);
            instance.addBee(new BeeWorker(new Transform(x, -4 - random.nextInt(4))));
        }
        for (int i = 0; i < 1 + random.nextInt(2); i++) {
            int x = 3 + random.nextInt(17);
            instance.addBee(new BeeDefender(new Transform(x, -4 - random.nextInt(4))));
        }
    }

    public void clearWorkers() {
        workers.clear();
    }
}
