package entity.abstract_entities;

import org.joml.Vector2f;
import org.joml.Vector3f;

import entity.Transform;
import io.Window;
import render.Camera;
import world.World;

public abstract class AbstractMovableEntity extends AbstractEntity {
    protected float speedX, speedY;
    protected float accelerationX, accelerationY;
    protected double absThreshold = 0.05;
    protected float speed;

    public AbstractMovableEntity(int maxAnimations, Transform transform) {
        super(maxAnimations, transform);
    }

    public void move(Vector2f direction) {
        transform.pos.add(new Vector3f(direction, 0));
        boundingBox.getCenter().set(transform.pos.x, transform.pos.y);
    }

    public void moveTo(Vector2f direction) {
        transform.pos.set(new Vector3f(direction, 0));
        boundingBox.getCenter().set(transform.pos.x, transform.pos.y);
    }

    public void flipLeft() {
        float x = transform.scale.x;
        float y = transform.scale.y;
        float z = transform.scale.z;
        if (x > 0)
            x = -x;
        transform.scale.set(new Vector3f(x, y, z));
    }

    public void flipRight() {
        float x = transform.scale.x;
        float y = transform.scale.y;
        float z = transform.scale.z;
        if (x < 0)
            x = -x;
        transform.scale.set(new Vector3f(x, y, z));
    }

    protected void updateTransform() {
        speedX += accelerationX;
        speedY -= accelerationY;
        move(new Vector2f(speedX, speedY));
    }

    public void rotate(float rotationZ) {
        transform.rotationZ = rotationZ;
    }

    public void update(float delta, Window window, Camera camera, World world) {
        updateTransform();
    }

}
