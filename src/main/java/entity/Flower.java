package entity;

import entity.abstract_entities.AbstractMovableEntity;
import io.Window;
import render.Animation;
import render.Camera;
import world.World;

public class Flower extends AbstractMovableEntity {
    public static final int ANIM_IDLE = 0;

    public Flower(Transform transform) {
        super(1, transform);
        setAnimation(ANIM_IDLE, new Animation(1, 8, "flower"));

        if (random.nextBoolean())
            flipLeft();
        else flipRight();

        transform.pos.add(-1 / 3 + random.nextFloat() / 3, 0, 0);
        float scale = random.nextFloat() / 5;
        transform.scale.add(scale, scale, 0);
        double angle = 15 + random.nextInt(9);
        if (random.nextBoolean())
            angle = Math.PI / angle;
        else angle = -Math.PI / angle;

        rotate((float) angle);
    }

    @Override
    public void update(float delta, Window window, Camera camera, World world) {
        useAnimation(ANIM_IDLE);
    }

}
