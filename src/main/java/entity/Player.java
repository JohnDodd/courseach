package entity;

import org.joml.Vector2f;

import entity.abstract_entities.AbstractMovableEntity;
import entity.bees.BeeWorker;
import entity.bees.Bees;
import io.Window;
import render.Animation;
import render.Camera;
import world.World;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_C;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_SPACE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;

public class Player extends AbstractMovableEntity {
    public static final int ANIM_IDLE = 0;
    public static final int ANIM_WALK = 1;
    public static final int ANIM_SIZE = 2;

    public Player(Transform transform) {
        super(ANIM_SIZE, transform);
        setAnimation(ANIM_IDLE, new Animation(4, 8, "player/idle"));
        setAnimation(ANIM_WALK, new Animation(4, 8, "player/walking"));
    }

    @Override
    public void update(float delta, Window window, Camera camera, World world) {
        Vector2f movement = new Vector2f();
        float angle = (float) Math.PI / 6;
        if (window.getInput().isKeyDown(GLFW_KEY_A)) {
            movement.add(-10 * delta, 0);
            flipLeft();
        }

        if (window.getInput().isKeyDown(GLFW_KEY_D)) {
            movement.add(10 * delta, 0);
            flipRight();
        }

        if (window.getInput().isKeyDown(GLFW_KEY_W)) {
            movement.add(0, 10 * delta);
            rotate(angle);
        }

        if (window.getInput().isKeyDown(GLFW_KEY_S)) {
            movement.add(0, -10 * delta);
            rotate(-angle);
        }
        if (!window.getInput().isKeyDown(GLFW_KEY_W) && !window.getInput().isKeyDown(GLFW_KEY_S)) {
            rotate(0);
        }

        float x = transform.pos.x;
        float y = transform.pos.y;

        if (window.getInput().isKeyPressed(GLFW_KEY_SPACE))
            Bees.getInstance().addBee(new BeeWorker(new Transform(x, y)));


        if (window.getInput().isKeyPressed(GLFW_KEY_C))
            Bees.getInstance().clearWorkers();


        if (x < 0) transform.pos.x = 0;
        if (x > 18) transform.pos.x = 18;
        if (-y < 0) transform.pos.y = 0;
        if (-y > 13) transform.pos.y = -13;

        move(movement);

        if (movement.x != 0 || movement.y != 0)
            useAnimation(ANIM_WALK);
        else
            useAnimation(ANIM_IDLE);
    }

}
