package entity;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Transform {
    public Vector3f pos;
    public Vector3f scale;
    public float rotationZ;

    public Transform() {
        pos = new Vector3f();
        scale = new Vector3f(1, 1, 1);
    }

    public Transform(float x, float y) {
        this();
        pos.x = x;
        pos.y = y;
    }

    public Transform(Transform transform) {
        this(transform.pos.x, transform.pos.y);
    }

    public Transform(Vector3f position) {
        scale = new Vector3f(1, 1, 1);
        pos = new Vector3f(position);
    }

    public Matrix4f getProjection(Matrix4f target) {
        target.translate(pos);
        target.scale(scale);
        target.rotate(rotationZ, new Vector3f(0.0f, 0.0f, 1.0f));
        return target;
    }
}
