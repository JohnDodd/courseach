package game;

import application.TimeUtils;
import assets.Assets;
import gui.Gui;
import io.Timer;
import io.Window;
import render.Camera;
import render.Shader;
import world.TileRenderer;
import world.World;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.opengl.GL.createCapabilities;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glViewport;

public class Main extends AbstractActivity {
    private Main() {
    }

    public static void main(String[] args) {
        new Main().run();
    }

    @Override
    protected void initInput() {
        keyboardListener.addAction(GLFW_KEY_ESCAPE, GLFW_PRESS, () ->
                glfwSetWindowShouldClose(window.getWindow(), true));

    }

    @Override
    protected void update() {
        float delta = TimeUtils.getInstance().getDeltaSeconds();
        world.update(delta, window, camera);
        world.correctCamera(camera, window);
        window.update();
    }


    @Override
    protected void render() {
        world.render(tileRenderer, shader, camera);
        gui.render();
    }

    public Main(int i) {
        Window.setCallbacks();

        if (!glfwInit()) {
            System.err.println("GLFW Failed to initialize!");
            System.exit(1);
        }

        Window window = new Window();
        window.setSize(640, 480);
        window.setFullscreen(false);
        window.createWindow("Game");

        createCapabilities();

        Camera camera = new Camera(window.getWidth(), window.getHeight());

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_TEXTURE_2D);

        TileRenderer tileRenderer = new TileRenderer();
        Assets.initAsset();
        Shader shader = new Shader(Shader.shader);

        World world = new World("test_level", camera);
        world.calculateView(window);

        Gui gui = new Gui(window);

        double frame_cap = 1.0 / 60.0;

        double frame_time = 0;
        int frames = 0;

        double time = Timer.getTime();
        double unprocessed = 0;

        while (!window.shouldClose()) {
            boolean can_render = false;

            double time_2 = Timer.getTime();
            double passed = time_2 - time;
            unprocessed += passed;
            frame_time += passed;

            time = time_2;

            while (unprocessed >= frame_cap) {
                if (window.hasResized()) {
                    camera.setProjection(window.getWidth(), window.getHeight());
                    gui.resizeCamera(window);
                    world.calculateView(window);
                    glViewport(0, 0, window.getWidth(), window.getHeight());
                }

                unprocessed -= frame_cap;
                can_render = true;

                System.out.println("frame_cap: " + frame_cap);
                world.update((float) frame_cap, window, camera);

                world.correctCamera(camera, window);

                window.update();

                if (frame_time >= 1.0) {
                    frame_time = 0;
                    //System.out.println("FPS: " + frames);
                    frames = 0;
                }
            }

            if (can_render) {
                glClear(GL_COLOR_BUFFER_BIT);

                world.render(tileRenderer, shader, camera);

                gui.render();

                window.swapBuffers();
                frames++;
            }
        }

        Assets.deleteAsset();

        glfwTerminate();
    }
}
