package game;

import application.TimeUtils;
import assets.Assets;
import gui.Gui;
import io.KeyboardListener;
import io.Window;
import render.Camera;
import render.Shader;
import world.TileRenderer;
import world.World;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.opengl.GL.createCapabilities;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glViewport;

public abstract class AbstractActivity {
    protected KeyboardListener keyboardListener;
    protected Camera camera;
    protected Shader shader;
    protected Window window;
    protected World world;
    protected Gui gui;
    protected TileRenderer tileRenderer;

    public void run() {
        initView();
        initInput();
        initLoop();
        exit();
    }

    protected void initView() {
        Window.setCallbacks();

        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");

        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        window = new Window();
        window.createWindow("Game");

        createCapabilities();

        camera = new Camera(window.getWidth(), window.getHeight());

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_TEXTURE_2D);

        tileRenderer = new TileRenderer();

        shader = new Shader(Shader.shader);
        Assets.initAsset();

        world = new World("test_level", camera);
        world.calculateView(window);

        gui = new Gui(window);

        keyboardListener = new KeyboardListener(window.getWindow());

        glfwSetWindowSizeCallback(window.getWindow(), (window1, width, height) -> {
            window.setSize(width, height);
            camera.setProjection(window.getWidth(), window.getHeight());
            gui.resizeCamera(window);
            world.calculateView(window);
            glViewport(0, 0, window.getWidth(), window.getHeight());
        });
    }

    protected abstract void initInput();

    protected void initLoop() {
        TimeUtils.getInstance();

        while (!window.shouldClose()) {
            TimeUtils.getInstance().update();
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            update();
            render();

            window.swapBuffers();
            glfwSwapInterval(1);
            glfwPollEvents();
        }
    }


    protected abstract void update();

    protected abstract void render();

    private void updateResizeWindow() {

    }

    private void exit() {
        glfwFreeCallbacks(window.getWindow());
        glfwDestroyWindow(window.getWindow());

        Assets.deleteAsset();

        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }
}
