package world;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import collision.AABB;
import entity.abstract_entities.AbstractMovableEntity;
import entity.Flower;
import entity.Player;
import entity.Transform;
import entity.bees.BeeEater;
import entity.bees.Bees;
import io.Window;
import render.Camera;
import render.Shader;

public class World {
    private int viewX;
    private int viewY;
    private byte[] tiles;
    private AABB[] bounding_boxes;
    private List<AbstractMovableEntity> entities;
    private List<Flower> flowers;
    private int width;
    private int height;
    private int scale;
    private Random random;

    private Matrix4f world;

    public World(String world, Camera camera) {
        //initWorld(world, camera);
        initMockWorld(camera);
    }

    private void initMockWorld(Camera camera) {
        width = 64;
        height = 64;
        scale = 32;

        tiles = new byte[width * height];
        bounding_boxes = new AABB[width * height];

        world = new Matrix4f().setTranslation(new Vector3f(0));
        world.scale(scale);
        entities = new ArrayList<>();
        flowers = new ArrayList<>();
        entities.add(new Player(new Transform()));
        fillEntities();

    }

    private void fillEntities() {
        random = new Random();
        /*int flowerSize = 7 + random.nextInt(2);
        List<Integer> has = new ArrayList<>();
        for (int i = 0; i < flowerSize; i++) {
            int x = (1 + random.nextInt(8)) * 2;
            while (true) {
                if (has.contains(x))
                    x = (1 + random.nextInt(8)) * 2;
                else break;
            }
            has.add(x);
        }

        System.out.println(has);

        for (int x : has) {
            flowers.add(new Flower(new Transform(x, -13)));
        }*/
        for (int x = 0; x < 10*2; x += 2)
            flowers.add(new Flower(new Transform(x, -13)));

        entities.addAll(flowers);
        Bees.getInstance().fill();
    }

    private void initWorld(String world, Camera camera) {

        try {
            BufferedImage tileSheet = ImageIO.read(new File("./levels/" + world + "/tiles.png"));
            BufferedImage entitySheet = ImageIO.read(new File("./levels/" + world + "/entities.png"));

            width = tileSheet.getWidth();
            height = tileSheet.getHeight();
            scale = 32;

            this.world = new Matrix4f().setTranslation(new Vector3f(0));
            this.world.scale(scale);

            int[] colorTileSheet = tileSheet.getRGB(0, 0, width, height, null, 0, width);
            int[] colorEntitySheet = entitySheet.getRGB(0, 0, width, height, null, 0, width);

            tiles = new byte[width * height];
            bounding_boxes = new AABB[width * height];
            entities = new ArrayList<>();

            Transform transform;

            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int red = (colorTileSheet[x + y * width] >> 16) & 0xFF;
                    int entityIndex = (colorEntitySheet[x + y * width] >> 16) & 0xFF;
                    int entityAlpha = (colorEntitySheet[x + y * width] >> 24) & 0xFF;

                    Tile tile;
                    try {
                        tile = Tile.tiles[red];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        tile = null;
                    }

                    if (tile != null)
                        setTile(tile, x, y);

                    if (entityAlpha > 0) {
                        transform = new Transform();
                        transform.pos.x = x * 2;
                        transform.pos.y = -y * 2;
                        switch (entityIndex) {
                            case 1:
                                entities.add(new Player(transform));
                                camera.getPosition().set(transform.pos.mul(-scale, new Vector3f()));
                                break;
                            default:

                                break;
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public World() {
        width = 64;
        height = 64;
        scale = 16;

        tiles = new byte[width * height];
        bounding_boxes = new AABB[width * height];

        world = new Matrix4f().setTranslation(new Vector3f(0));
        world.scale(scale);
    }

    public void calculateView(Window window) {
        viewX = (window.getWidth() / (scale * 2)) + 4;
        viewY = (window.getHeight() / (scale * 2)) + 4;
    }

    public Matrix4f getWorldMatrix() {
        return world;
    }

    public void render(TileRenderer render, Shader shader, Camera cam) {
        int posX = (int) cam.getPosition().x / (scale * 2);
        int posY = (int) cam.getPosition().y / (scale * 2);

        for (int i = 0; i < viewX; i++) {
            for (int j = 0; j < viewY; j++) {
                Tile t = getTile(i - posX - (viewX / 2) + 1, j + posY - (viewY / 2));
                if (t != null)
                    render.renderTile(t, i - posX - (viewX / 2) + 1, -j - posY + (viewY / 2), shader, world, cam);
            }
        }

        Bees.getInstance().render(shader, cam, this);
        for (AbstractMovableEntity entity : entities) {
            entity.render(shader, cam, this);
        }
    }

    public void update(float delta, Window window, Camera camera) {
        for (AbstractMovableEntity entity : entities)
            entity.update(delta, window, camera, this);
        Bees.getInstance().update(delta, window, camera, this);
    }

    public void correctCamera(Camera camera, Window window) {
        Vector3f pos = camera.getPosition();

        int w = -width * scale * 2;
        int h = height * scale * 2;

        if (pos.x > -(window.getWidth() / 2) + scale)
            pos.x = -(window.getWidth() / 2) + scale;
        if (pos.x < w + (window.getWidth() / 2) + scale)
            pos.x = w + (window.getWidth() / 2) + scale;

        if (pos.y < (window.getHeight() / 2) - scale)
            pos.y = (window.getHeight() / 2) - scale;
        if (pos.y > h - (window.getHeight() / 2) - scale)
            pos.y = h - (window.getHeight() / 2) - scale;
    }

    public void setTile(Tile tile, int x, int y) {
        tiles[x + y * width] = tile.getId();
        if (tile.isSolid()) {
            bounding_boxes[x + y * width] = new AABB(new Vector2f(x * 2, -y * 2), new Vector2f(1, 1));
        } else {
            bounding_boxes[x + y * width] = null;
        }
    }

    public Tile getTile(int x, int y) {
        try {
            return Tile.tiles[tiles[x + y * width]];
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    public AABB getTileBoundingBox(int x, int y) {
        try {
            return bounding_boxes[x + y * width];
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    public BeeEater getEater() {
        return Bees.getInstance().getEater();
    }

    public Flower getRandomFlower() {
        int x = random.nextInt(flowers.size() - 1);
        return flowers.get(x);
    }
}
