package io;

import java.util.LinkedList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;

public class KeyboardListener {
    private long window;
    private List<Action> actions;

    public KeyboardListener(long window) {
        this.window = window;
        actions = new LinkedList<>();
        initCallbacks();
    }

    public KeyboardListener() {
    }

    private void initCallbacks() {
        glfwSetKeyCallback(window, (window1, key, scancode, keyAction, mods) -> {
            for (Action action : actions) {
                if (action.getKey() == key && action.contains(keyAction))
                    action.act();
            }
        });
    }

    public KeyboardListener addAction(int key, int keyAction, ActionI action) {
        actions.add(new Action(key, keyAction) {
            @Override
            public void act() {
                action.act();
            }
        });

        return this;
    }

    public KeyboardListener addAllActions(List<Action> actions){
        this.actions.addAll(actions);
        return this;
    }

    public KeyboardListener addAction(int key, Integer keyAction, Integer keyAction1, ActionI action) {
        List<Integer> list = new LinkedList<>();
        list.add(keyAction);
        list.add(keyAction1);
        actions.add(new Action(key, list) {
            @Override
            public void act() {
                action.act();
            }
        });
        return this;
    }

    public KeyboardListener addAction(int key, List<Integer> keyActions, ActionI action) {
        actions.add(new Action(key, keyActions) {
            @Override
            public void act() {
                action.act();
            }
        });
        return this;
    }

    public List<Action> getActions() {
        return actions;
    }

    public static abstract class Action implements ActionI {
        private int key;
        private List<Integer> keyActions;

        public Action(int key, int... keyAction) {
            this.key = key;
            keyActions = new LinkedList<>();
            for (int k : keyAction)
                keyActions.add(k);

        }

        public Action(int key, List<Integer> keyAction) {
            this.key = key;
            keyActions = keyAction;
        }

        public int getKey() {
            return key;
        }

        public boolean contains(Integer action) {
            return keyActions.contains(action);
        }
    }

    public interface ActionI {
        void act();
    }
}
